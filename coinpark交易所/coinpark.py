# -*- coding: utf-8 -*-

import time
import hashlib
import requests
import urllib
import copy
import operator
import json
import hmac

class cc():
    
    _headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
	'Accept': 'application/json'
    }

    def __init__(self, api_Key, secret_key):
        self.url = 'https://api.coinpark.cc'
        self.api_Key = api_Key
        self.secret_key = secret_key
    def sign(self,params):
        # out = []
        # for k, v in kwargs.items():
        #     out.append("{}={}".format(str(k).strip(), str(v).strip()))
        # out.sort()
        # s = "&".join(out)
        return hmac.new(self.secret_key.encode(), params.encode(), digestmod=hashlib.md5).hexdigest()
    
    def get(self,url,params):
        headers = dict(self._headers)
        postdata = urllib.parse.urlencode(params)
        try:
            response = requests.get(url, postdata, headers=headers, timeout=5)
            if response.status_code == 200:
                data = response.json()
                return data
            else:
                print( {"status":"fail"},response.status_code)
        except Exception as e:
            print("httpGet failed, detail is:%s" %e)
    
    def postt(self,url,params):
        headers = dict(self._headers)
        postdata =urllib.parse.urlencode(params)
        try:
            response = requests.post(url,postdata, headers=headers, timeout=5)
            if response.status_code == 200:
                data = response.json()
                if data.get('code') !=  200 :
                    print(data.get('info'),data.get('code'))
                return data
            else:
                print( {"status":"fail"},response.status_code)
        except Exception as e:
            print("httpGet failed, detail is:%s" %e)
    
    def get_depth(self,pair,size):
        dit={}
        dit['pair']= pair
        dit['size']=size
        url = self.url+'/v1/mdata?cmd=depth'
        return self.get(url,dit)
        
    def take_order(self,pair,order_side,price,amount):
        data={}
        cmds=   [{
        'cmd':"orderpending/trade",
        'body':{
            'pair':pair,
            'account_type':0,
            'order_type':2,
            'order_side':order_side,
            'price':price,
            'amount':amount
             }
          }]
        data['cmds']=json.dumps(cmds)
        data['apikey']=self.api_Key
        data['sign']=self.sign(json.dumps(cmds))
        url = self.url+'/v2/orderpending'
        return self.postt(url,data)
    def get_balance(self):
        data={}
        cmds=[{
           "cmd": "transfer/assets",
           "body": {"select": 1}
            }]
        data['cmds']=json.dumps(cmds)
        data['apikey']=self.api_Key
        data['sign']=self.sign(json.dumps(cmds))
        url =self.url+'/v1/transfer'
        return self.postt(url,data)
        
    def cancel_order(self,idd):
        data={}
        cmds=   [{
        'cmd':"orderpending/cancelTrade",
        'index':12345,
        'body':{
            'orders_id':idd
             }
          }]
        data['cmds']=json.dumps(cmds)
        data['apikey']=self.api_Key
        data['sign']=self.sign(json.dumps(cmds))
        url = self.url+'/v2/orderpending'
        return self.postt(url,data)
    
        
        

