# -*- coding: utf-8 -*-

import time
import hashlib
import requests
import urllib
import copy
import operator
import json
import hmac
import base64
import binascii

#每2秒6个请求


class Bihe():
    
    _headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
	'Accept': 'application/json',
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'
    }
    def __init__(self, api_Key, secret_key,api_Key1,secret_key1):
        self.url = 'https://www.senbit.com' 
        self.api_Key = api_Key
        self.secret_key = secret_key
        self.api_Key1 = api_Key1
        self.secret_key1 = secret_key1
        
    def sign(self,params):
        params=copy.copy(params)
        now = int(time.time()*1000)
        params['_']= now
        params['method']='GET'
        params['access']=self.api_Key
        sort_params = sorted(params.items(), key=operator.itemgetter(0))
        params = dict(sort_params)
        string = urllib.parse.urlencode(params).encode('utf-8')
        data = self.secret_key.encode('utf-8')
        sign = hmac.new(data,string,hashlib.sha256).hexdigest()
        params={}
        params['_'] = now
        params['access'] = self.api_Key
        params['sign'] = sign
        return params
    
    def sign1(self,params):
        params=copy.copy(params)
        now = int(time.time()*1000)
        params['_']= now
        params['method']='POST'
        params['access']=self.api_Key1
        sort_params = sorted(params.items(), key=operator.itemgetter(0))
        params = dict(sort_params)
        string = urllib.parse.urlencode(params).encode('utf-8')
        data = self.secret_key1.encode('utf-8')
        sign = hmac.new(data,string,hashlib.sha256).hexdigest()
        params={}
        params['_'] = now
        params['access'] = self.api_Key1
        params['sign'] = sign
        return params
            
    def get_sign(self,url,params):
        _headers = dict(self._headers)
        postdata = urllib.parse.urlencode(params)
        try:
            response = requests.get(url, postdata, headers=_headers, timeout=5)
            if response.status_code == 200:
                data = response.json()
                return data
            else:
                print( {"status":"fail"},response.status_code)
        except Exception as e:
            print("httpGet failed, detail is:%s" %e)

    def get_balance(self):
        params={}
        params['path'] = '/api/x/v1/account/balance'
        sign =self.sign(params)
        a={}
        dit = dict(a,**sign)
        url = self.url + '/api/x/v1/account/balance?'
        # print(dit)
        return self.get_sign(url,dit)
        
    def get_depth(self,symbol):
        params={}
        params['symbol']=symbol
        params['path']='/api/x/v1/market/depth'
        sign =self.sign(params)
        a={}
        a['symbol']=symbol
        dit = dict(a,**sign)
        url = self.url + '/api/x/v1/market/depth?'        
        return self.get_sign(url,dit)
    
    def get_ticker(self,symbol):
        params={}
        params['symbol']=symbol
        params['path']='/api/x/v1/market/tickers'
        sign =self.sign(params)
        a={}
        a['symbol']=symbol
        dit = dict(a,**sign)
        url = self.url + '/api/x/v1/market/tickers?'        
        return self.get_sign(url,dit)
    
    def take_order(self,symbol,type1,price,amount):
        params={}
        params['path']='/api/x/v1/order/order'
        sign=self.sign1(params)
        a={}
        a['symbol']=symbol
        a['type']=type1
        a['price']=price
        a['amount']=amount
        url = self.url + '/api/x/v1/order/order'
        x=requests.request('POST', url, params=sign, data=a, headers=self._headers)
        return  x.json()
    
    def cancel_order(self,idd,symbol):
        params={}
        params['path']= '/api/x/v1/order/order/{id}'
        sign = self.sign1(params)
        a={}
        a['id']=idd
        a['symbol']=symbol
        url = self.url + '/api/x/v1/order/order/{id}'
        x=requests.request('POST', url, params=sign, data=a, headers=self._headers)
        return  x.status_code
        
        
        
        
        
        
        