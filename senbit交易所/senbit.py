import hmac
import time
import hashlib
import requests
from urllib.parse import quote_plus
from pprint import pprint as pp

SITE = 'https://www.senbit.com'
KEY_COMMON_ACCESS = '1H2QMV4f8EHjA93q6xc7QF'
KEY_COMMON_SECRET = '2OXQgE0S2XVoQ6mkeqDDf5'
KEY_TRADE_ACCESS = '1LBSLh2QJQa1Y8S1xxLNZA'
KEY_TRADE_SECRET = '1exiVi8p1vPAFabDvQgRM1'

HEADERS = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
}

def get_time_str():
    return str(int(time.time()*1000))

def get_quote(_):
    return quote_plus(_)

def get_sign(time_str, path, method, market):
    sign_query = '_=%s&access=%s&method=%s&path=%s&symbol=%s'%(time_str, KEY_COMMON_ACCESS, method, get_quote(path), get_quote(market))
    sign_result = hmac.new(bytes(KEY_COMMON_SECRET, 'utf-8'), bytes(sign_query, 'utf-8'), digestmod=hashlib.sha256).hexdigest()

    return sign_result

def get_sign_place_order(time_str, path, method):
    sign_query = '_=%s&access=%s&method=%s&path=%s'%(time_str, KEY_TRADE_ACCESS, method, get_quote(path))
    sign_result = hmac.new(bytes(KEY_TRADE_SECRET, 'utf-8'), bytes(sign_query, 'utf-8'), digestmod=hashlib.sha256).hexdigest()

    return sign_result

def get_sign_cancel_order(time_str, path, method, market):
    sign_query = '_=%s&access=%s&method=%s&path=%s&symbol=%s'%(time_str, KEY_TRADE_ACCESS, method, get_quote(path), get_quote(market))
    sign_result = hmac.new(bytes(KEY_TRADE_SECRET, 'utf-8'), bytes(sign_query, 'utf-8'), digestmod=hashlib.sha256).hexdigest()

    return sign_result

def call_api_timestamp():
    path = '/api/x/v1/common/timestamp'
    url = '%s%s'%(SITE, path)

    _ = requests.get(url, headers=HEADERS)
    ret = {
        "status_code": _.status_code,
        "json": _.json()
    }
    return ret

def call_api_tickers(market):
    time_str = get_time_str()

    method = 'GET'
    path = '/api/x/v1/market/tickers'
    url = '%s%s'%(SITE, path)

    sign = get_sign(time_str, path, method, market)
    params = {
        '_': time_str,
        'access': KEY_COMMON_ACCESS,
        'sign': sign,
        'symbol': market,
    }

    _ = requests.get(url, params=params, headers=HEADERS)
    ret = {
        "status_code": _.status_code,
        "json": _.json()
    }
    return ret

def call_api_place_order(symbol, _type, price, amount):
    time_str = get_time_str()

    method = 'POST'
    path = '/api/x/v1/order/order'
    url = '%s%s'%(SITE, path)

    sign = get_sign_place_order(time_str, path, method)
    params = {
        '_': time_str,
        'access': KEY_TRADE_ACCESS,
        'sign': sign,
    }
    post_data = {
        'symbol': symbol,
        'type': _type,
        'price': price,
        'amount': amount,
    }

    _ = requests.request(method, url, params=params, data=post_data, headers=HEADERS)
    ret = {
        "status_code": _.status_code,
        "json": _.json()
    }
    return ret


def call_api_cancel_order(symbol, orderid):
    time_str = get_time_str()

    method = 'DELETE'
    path = '/api/x/v1/order/order/{}'.format(orderid)
    url = '%s%s'%(SITE, path)

    sign = get_sign_cancel_order(time_str, path, method, symbol)
    params = {
        '_': time_str,
        'access': KEY_TRADE_ACCESS,
        'sign': sign,
        'symbol': symbol,
    }

    _ = requests.request(method, url, params=params, headers=HEADERS)
    ret = {
        "status_code": _.status_code,
    }
    return ret

if __name__ == "__main__":
    # ret = call_api_timestamp()
#     pp(ret)
#     print('\n')

#     ret = call_api_tickers('SGT/USDT')
#     pp(ret)
#     print('\n')

    symbol = 'XRP/USDT'
    _type = 'buy'
    price = '0.22'
    amount = '10'
    ret = call_api_place_order(symbol, _type, price, amount)
    pp(ret)
    print(ret)

#     orderid = ret.get('json', {}).get('orderid')
#     assert orderid is not None

#     ret = call_api_cancel_order(symbol, orderid)
#     pp(ret)
#     status_code = ret.get('status_code')
#     if status_code == 201:
#         print('cancel order success.')
#     else:
#         print('cancel order fail.')
